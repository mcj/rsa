import java.math.BigInteger;
import java.util.Scanner;
import java.security.SecureRandom;

import static java.math.BigInteger.*;

public class MyRSA {
    private static final int BIT_DEPTH = 512;
    private static final SecureRandom random = new SecureRandom();

    static BigInteger greatestCommonDivisor(BigInteger p1, BigInteger p2) {
        if (p2.equals(ZERO))
            throw new IllegalArgumentException("p2 is 0");

        BigInteger n1 = p1;
        BigInteger n2 = p2;

        while (true) {
            BigInteger quotient = n1.divide(n2);
            BigInteger remainder = n1.remainder(n2);

            if (remainder.equals(ZERO))
                return n2;

            n1 = n2;
            n2 = remainder;
        }
    }

    static RSAKeyPair generateKeyPair() {
        BigInteger p = BigInteger.probablePrime(BIT_DEPTH, random);
        BigInteger q = BigInteger.probablePrime(BIT_DEPTH, random);

        BigInteger minusOneProduct = p.subtract(ONE).multiply(q.subtract(ONE));
        BigInteger randomE;
        do {
            randomE = new BigInteger(BIT_DEPTH, random);
        } while (! greatestCommonDivisor(randomE, minusOneProduct).equals(ONE));

        RSAPrivateKey priv = new RSAPrivateKey(p, q, randomE);
        RSAPublicKey pub = new RSAPublicKey(p.multiply(q), randomE);
        return new RSAKeyPair(priv, pub);
    }

    public static BigInteger encode(BigInteger clear, RSAPublicKey pubKey) {
        return clear.modPow(pubKey.e, pubKey.n);
    }

    public static BigInteger decode(BigInteger secret, RSAPrivateKey key) {
        return secret.modPow(key.d, key.n);
    }

    public static BigInteger stringToInt(String s) {
        return new BigInteger(1, s.getBytes());
    }

    public static String intToString(BigInteger i) {
        return new String(i.toByteArray());
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            help();
        }

        Scanner sc = new Scanner(System.in);

        if (args[0].equals("genkey")) {
            RSAKeyPair keys = generateKeyPair();
            System.out.println("Public: " + keys.publicKey);
            System.out.println("Private: " + keys.privateKey);
        } else if (args[0].equals("encode")) {
            System.out.print("Public Key N: ");
            BigInteger n = new BigInteger(sc.nextLine());
            System.out.print("Public Key E: ");
            BigInteger e = new BigInteger(sc.nextLine());
            RSAPublicKey pubKey = new RSAPublicKey(n, e);

            System.out.print("Text: ");
            String text = sc.nextLine();

            BigInteger textInt = stringToInt(text);

            System.out.println(encode(textInt, pubKey));
        } else if (args[0].equals("decode")) {
            System.out.print("Private Key P: ");
            BigInteger p = new BigInteger(sc.nextLine());
            System.out.print("Private Key Q: ");
            BigInteger q = new BigInteger(sc.nextLine());
            System.out.print("Private Key E: ");
            BigInteger e = new BigInteger(sc.nextLine());
            RSAPrivateKey privKey = new RSAPrivateKey(p, q, e);

            System.out.print("Encoded Text: ");
            BigInteger text = new BigInteger(sc.nextLine());

            BigInteger decoded = decode(text, privKey);
            System.out.println(intToString(decoded));
        } else {
            help();
        }
    }

    private static void help() {
        System.out.println("genkey | encode | decode");
        System.exit(1);
    }

    static class RSAKeyPair {
        RSAPrivateKey privateKey;
        RSAPublicKey publicKey;

        RSAKeyPair(RSAPrivateKey priv, RSAPublicKey pub) {
            privateKey = priv;
            publicKey = pub;
        }
    }

    static class RSAPrivateKey {
        // (probably) prime numbers:
        BigInteger p;
        BigInteger q;

        BigInteger d;
        BigInteger e;
        BigInteger n;

        RSAPrivateKey(BigInteger p, BigInteger q, BigInteger e) {
            this.n = p.multiply(q);

            BigInteger minusOneProduct = p.subtract(ONE).multiply(q.subtract(ONE));
            this.d = e.modInverse(minusOneProduct);
            this.p = p;
            this.q = q;
            this.e = e;
        }

        @Override
        public String toString() {
            return "p=" + p + ", q=" + q + ", e=" + e + ", d=" + d;
        }
    }

    static class RSAPublicKey {
        BigInteger n;
        BigInteger e;

        RSAPublicKey(BigInteger n, BigInteger e) {
            this.n = n;
            this.e = e;
        }

        @Override
        public String toString() {
            return "n=" + n + ", e=" + e;
        }
    }
}
